﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcRides.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;

namespace MvcRides.Controllers
{
    public class PassengerController : Controller
    {
        private RideCMSContext db = new RideCMSContext();

        // GET: Passenger
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {

            return View(db.Passengers.ToList());
        }

        public ActionResult New()
        {
            //no information needed from db

            return View();
        }

        //Create new passenger
        [HttpPost]
        public ActionResult Create(string P_FName, string P_LName, string P_Phone, string P_Email)
        {

            string query = "insert into passengers (P_FName, P_LName, P_Phone, P_Email) values (@P_FName, @P_LName, @P_Phone, @P_Email)";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@P_FName", P_FName);
            myparams[1] = new SqlParameter("@P_LName", P_LName);
            myparams[2] = new SqlParameter("@P_Phone", P_Phone);
            myparams[3] = new SqlParameter("@P_Email", P_Email);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        // GET: Passenger/Edit/5
        public ActionResult Edit(int? id)
        {
            Debug.WriteLine("testing passenger edit");
            if ((id == null) || (db.Passengers.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from passengers where PassengerID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Passenger mypassenger = db.Passengers.SqlQuery(query, param).FirstOrDefault();
            return View(mypassenger);
        }
        // POST: passengers/Edit/5

        [HttpPost]

        public ActionResult Edit(int id, string P_FName, string P_LName, string P_Phone, string P_Email)
        {

            if ((id == null) || (db.Passengers.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update passengers set P_FName=@P_FName, P_LName=@P_LName, P_Phone = @P_Phone, P_Email = @P_Email where PassengerID = @id";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@P_FName", P_FName);
            myparams[2] = new SqlParameter("@P_LName", P_LName);
            myparams[3] = new SqlParameter("@P_Phone", P_Phone);
            myparams[4] = new SqlParameter("@P_Email", P_Email);


            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        //delete Passenger
        // GET: Passengers/Delete


        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Passengers.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from passengers where PassengerID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");

        }
    }
}
