﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcRides.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;


namespace MvcRides.Controllers
{
    public class DriverController : Controller

    {
        private RideCMSContext db = new RideCMSContext();
        // GET: Driver
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {

            return View(db.Drivers.ToList());
        }

        public ActionResult New()
        {
            //no information needed from db

            return View();
        }
        // Create new driver
        [HttpPost]
        public ActionResult Create(string D_FName, string D_LName, string D_Age, string D_Email, string D_Phone)
        {
            //actually ask to put info into db

            string query = "insert into drivers (D_FName, D_LName, D_Age, D_Email, D_Phone) values (@D_FName, @D_LName, @D_Age, @D_Email, @D_Phone)";
            SqlParameter[] myparams = new SqlParameter[5];
            myparams[0] = new SqlParameter("@D_FName", D_FName);
            myparams[1] = new SqlParameter("@D_LName", D_LName);
            myparams[2] = new SqlParameter("@D_Age", D_Age);
            myparams[3] = new SqlParameter("@D_Email", D_Email);
            myparams[4] = new SqlParameter("@D_Phone", D_Phone);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        // GET: Driver/Edit/5
        public ActionResult Edit(int? id)
        {
            Debug.WriteLine("testing driver edit");
            if ((id == null) || (db.Drivers.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from drivers where DriverID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Driver mydriver = db.Drivers.SqlQuery(query, param).FirstOrDefault();
            return View(mydriver);
        }
        // POST: drivers/Edit/5

        [HttpPost]

        public ActionResult Edit(int id, string D_FName, string D_LName, string D_Age, string D_Email, string D_Phone)
        {

            if ((id == null) || (db.Drivers.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update drivers set D_FName=@D_FName, D_LName=@D_LName, D_Age = @D_Age, D_Email = @D_Email, D_Phone=@D_Phone where DriverID = @id";
            SqlParameter[] myparams = new SqlParameter[6];
            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@D_FName", D_FName);
            myparams[2] = new SqlParameter("@D_LName", D_LName);
            myparams[3] = new SqlParameter("@D_Age", D_Age);
            myparams[4] = new SqlParameter("@D_Email", D_Email);
            myparams[5] = new SqlParameter("@D_Phone", D_Phone);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        //delete driver
        // GET: Drivers/Delete


        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Drivers.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from Drivers where DriverID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");

        }
    }
}