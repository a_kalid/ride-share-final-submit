﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcRides.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;

namespace MvcRides.Controllers
{
    public class RideController : Controller
    {
        private RideCMSContext db = new RideCMSContext();
        // GET: Ride
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {

            return View(db.Rides.ToList());
        }

        public ActionResult New()
        {
            //no information needed from db

            return View();
        }
        //create new Ride
        [HttpPost]

        public ActionResult Create(string departureCity, string arrivalCity, string pickSpot, string dropSpot, string distance, string departureDate, string departureTime, int price)
        {
            //actually ask to put info into db

            string query = "insert into Rides (departureCity, arrivalCity, pickSpot, dropSpot, distance, departureDate, departureTime, price) values (@departureCity, @arrivalCity, @pickSpot, @dropSpot, @distance, @departureDate, @departureTime, @price)";
            SqlParameter[] myparams = new SqlParameter[8];
            myparams[0] = new SqlParameter("@departureCity", departureCity);
            myparams[1] = new SqlParameter("@arrivalCity", arrivalCity);
            myparams[2] = new SqlParameter("@pickSpot", pickSpot);
            myparams[3] = new SqlParameter("@dropSpot", dropSpot);
            myparams[4] = new SqlParameter("@distance", distance);
            myparams[5] = new SqlParameter("@departureDate", departureDate);
            myparams[6] = new SqlParameter("@departureTime", departureTime);
            myparams[7] = new SqlParameter("@price", price);


            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }

        // GET: ride/Edit/5
        public ActionResult Edit(int? id)
        {

            if ((id == null) || (db.Rides.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from rides where RideID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Ride myride = db.Rides.SqlQuery(query, param).FirstOrDefault();
            return View(myride);
        }
        // POST: ride/Edit/5

        [HttpPost]

        public ActionResult Edit(int id, string departureCity, string arrivalCity, string pickSpot, string dropSpot, string distance, string departureDate, string departureTime, int price)
        {

            if ((id == null) || (db.Rides.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update rides set departureCity=@departureCity, arrivalCity=@arrivalCity, pickSpot = @pickSpot, dropSpot=@dropSpot, distance=@distance, departureDate=@departureDate, departureTime = @departureTime, price=@price  where RideID = @id";
            SqlParameter[] myparams = new SqlParameter[9];
            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@departureCity", departureCity);
            myparams[2] = new SqlParameter("@arrivalCity", arrivalCity);
            myparams[3] = new SqlParameter("@pickSpot", pickSpot);
            myparams[4] = new SqlParameter("@dropSpot", dropSpot);
            myparams[5] = new SqlParameter("@distance", distance);
            myparams[6] = new SqlParameter("@departureDate", departureDate);
            myparams[7] = new SqlParameter("@departureTime", departureTime);
            myparams[8] = new SqlParameter("@price", price);


            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        //delete Ride
        // GET: Rides/Delete


        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Rides.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from rides where RideID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");

        }
    }
}

