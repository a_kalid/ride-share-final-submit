﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;


namespace MvcRides.Models
{
    public class Ride
    {
        [Key]
        public int RideID { get; set; }

        public string departureCity { get; set; }

        public string arrivalCity { get; set; }

        public string pickSpot { get; set; }

        public string dropSpot { get; set; }

        public string distance { get; set; }

        public String departureDate { get; set; }

        public String departureTime { get; set; }

        public int price { get; set; }

        public virtual ICollection<Passenger> passengers { get; set; }
    }
}