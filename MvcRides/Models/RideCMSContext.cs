﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MvcRides.Models
{
    public class RideCMSContext : DbContext
    {
        public RideCMSContext()
        { 

        }
        public DbSet<Driver>Drivers { get; set; }
        public DbSet<Ride>Rides { get; set; }
        public DbSet<Passenger>Passengers { get; set; }
    }
}