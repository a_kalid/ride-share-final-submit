﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcRides.Models
{
    public class Driver
    {
        [Key]
        public int DriverID { get; set; }


        public string D_FName { get; set; }


        public string D_LName { get; set; }


        public string D_Age { get; set; }

        
        public string D_Email { get; set; }


        public string D_Phone { get; set; }


        //public string Drivers { get; set; }

        public virtual ICollection<Passenger> Passengers { get; set; }
        

       





    }

}