﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace MvcRides.Models
{
    public class Passenger
    {
        [Key]
        public int PassengerID { get; set; }

        public string P_FName { get; set; }

        public string P_LName { get; set; }

        public string P_Phone { get; set; }

        public string P_Email { get; set; }

        public virtual Ride Ride { get; set; }

    }
}